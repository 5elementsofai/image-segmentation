from keras_segmentation.models.all_models import model_from_name

model_config = {
    "input_height": 473,
    "input_width": 473,
    "n_classes": 150,
    "model_class": "pspnet_50",
}

model_class = model_config['model_class']
Model = model_from_name[model_class]
model = Model(
    model_config['n_classes'], 
    input_height=model_config['input_height'],
    input_width=model_config['input_width']
)
model.load_weights("./models/pspnet50_ade20k.h5")

out = model.predict_segmentation(
    inp="./tmp/input.jpg",
    out_fname="./tmp/output.png"
)