import datetime
from flask import Flask, jsonify, request, send_file
from flask_cors import CORS, cross_origin
from keras_segmentation.models.all_models import model_from_name
from werkzeug.utils import secure_filename
import os

UPLOAD_FOLDER = './uploads'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

cors = CORS(app)

required_directories = ['./tmp', './uploads']
for required_directory in required_directories:
    if not os.path.exists(required_directory):
        os.makedirs(required_directory)


def build_model():
    model_config = {
        "input_height": 473,
        "input_width": 473,
        "n_classes": 150,
        "model_class": "pspnet_50",
    }

    model_class = model_config['model_class']
    Model = model_from_name[model_class]
    model = Model(
        model_config['n_classes'], 
        input_height=model_config['input_height'],
        input_width=model_config['input_width']
    )
    model.load_weights("./models/pspnet50_ade20k.h5")
    
    return model


API_V1 = '/api/1.0'

@app.route(API_V1 + '/ping', methods=['GET'])
def ping():
    return "pong"

@app.route(API_V1 + '/info', methods=['GET'])
def info():
    return jsonify({
        'version': API_V1,
        'project': '5 elements of AI',
        'service': 'image-segmentation',
        'language': 'python',
        'type': 'api',
        'date': str(datetime.datetime.now()),
    })


@app.route(API_V1 + '/predict', methods=['POST', 'OPTIONS'])
@cross_origin(origin='localhost')
def predict():
    print('files ', request.files)
    if 'image' in request.files:
        file = request.files['image']
        input_file = secure_filename(file.filename)
        target_file = './uploads/' + input_file
        file.save(target_file)

        output_file = "./tmp/" + os.path.basename(input_file) + ".png"
        
        model = build_model()
        out = model.predict_segmentation(
            inp=target_file,
            out_fname=output_file
        )

        return send_file(output_file, mimetype='image/png')
    
    return 'no image provided'
    
    

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True, threaded=True)